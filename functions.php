<?php
  //自動更新css
  add_action( 'wp_enqueue_scripts', 'enqueue_child_theme_styles', PHP_INT_MAX);
  function enqueue_child_theme_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
    wp_enqueue_style( 'child-style', get_stylesheet_uri(), NULL, filemtime( get_stylesheet_directory().'/style.css' ) );
  }
  //首頁主選單
  register_nav_menus(
    array(
    'header-menu' => __( '主選單' ),
    'nav-menu'=>__('導覽列'),
    'allNewsBlock'=>__('所有公告'),
    'staffInform'=>__('人員簡介'),
    'publicBlock'=>__('公開專區'),
    'worldRank' =>__('世界大學排名'),
    'relatedUrl_government'=>__('相關連結-政府單位'),
    'relatedUrl_campus'=>__('相關連結-校務單位'),
    'relatedUrl_enterprise'=>__('相關連結-企業組織')
    )
  );
  //特色圖片功能
  if ( function_exists( 'add_theme_support' ) ) {
    add_theme_support( 'post-thumbnails' );
  }
  //取得文章圖片
  function get_feature_image(){
    global $post,  $posts;
    $first_img = '';
    if ( has_post_thumbnail() ) {
      $first_img = wp_get_attachment_url( get_post_thumbnail_id() );
    } else {
      ob_start();
      ob_end_clean();
      $output = preg_match('/< *img[^>]*src *= *["\']?([^"\']*)/i', $post->post_content, $matches);
      $first_img = $matches[1];
    }
    if($first_img)echo $first_img;
  }
  //頁碼
  $pageIndex=0;
  $pageId=0;
  if($_GET['pageIndex'])$pageIndex=$_GET['pageIndex'];
  if($_GET['pageId'])$pageId=$_GET['pageId'];
  function wp_pagenavi($pageIndex) {
    global $wp_query;
    $max = $wp_query->max_num_pages;
    if ( !$current = get_query_var('paged') ) $current =1;
    $args['base'] = str_replace(999999999, '%#%',get_pagenum_link(999999999));
    $args['total'] = $max;
    $args['current'] = $current;
    $args['prev_text'] = '<';
    $args['next_text'] = '>';
    if ( $max > 1 ){ 
      $pages = '<div
      class="wp-pagenavi"><span class="pages">共 ' . $max . '
      頁</span>';
      for($i=1; $i<=$max; $i++){
        echo '<a href=?pageIndex='.$pageIndex.'&pageId='.$i.'>'.$i.'</a> '; //127.0.0.1?id='.$id.'&pageId='.$i.'
      }
    }
  }

  // tag_id (所有分類, 校務, 校內, 校外, 電子報)
  //$allId=array(11, 12, 13, 14, 15);
  $allId=array(5, 5, 5, 5, 5);

  //哪個分類的文章
  function whichCat($id, $paged, $showposts){
    //$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    query_posts('paged='.$paged.' & cat='.$id.' & showposts='.$showposts);
    $index=0; 
    while(have_posts()):the_post();
    $index++;
    echo "<span class='newsPutDate' style='left:10px'>";
    the_time("Y.n.j");
    echo "</span>";
    echo "<li><a href=".esc_url( apply_filters( 'the_permalink', get_permalink( $post ), $post ) )."?back=2>";
    if($index==1)echo "<span class=newsPutTop>置頂</span>";
    echo the_title()."</a></li>";
    endwhile;
  }

  //各種 get_post
  $rootDir=get_bloginfo('template_directory');
  include 'pageID.php';

?>
