<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <title>搜尋結果</title>
  <script src="<? bloginfo('template_directory')?>/js/jquery-3.3.1.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <style type="text/css">
    ::-webkit-scrollbar {
      width: 3px;
    }
    ::-webkit-scrollbar-track {
      background-color: black;
    }
    ::-webkit-scrollbar-thumb {
      background-color: white; 
    }
    @font-face{
      font-family:'Noto Sans CJK TC';
      src:url('http://127.0.0.1/wp-content/themes/bigdata/assets/NotoSansMonoCJKtc-Bold.otf')
    }
    html,body{
      overflow:hidden;
      font-family:'Noto Sans CJK TC';
      background-color:#005CAF;
    }
    .searchLeftside{
      position:absolute;
      top:0px;
      left:0px;
      width:25%;
      height:100vh;
      background-color:#eb9f52;
    }
    .searchTitle{
      color:white;
      font-size:35px;
      position:absolute;
      left:15%;
      top:15vh;
    }
    .line{
      position:absolute;
      top:25vh;
      left:15%;
      width:100%;
      height:2px;
      background-color:white;
    }
    .keyword{
      color:white;
      position:absolute;
      top:30vh;
      left:18%;
      width:75%;
      border:2px solid white;
    }
    .search{
      color:white;
      opacity:0.5;
      font-size:20px;
      position:absolute;
      left:60%;
      top:18vh;
      cursor:pointer;
    }
    .searchBlank{
      background-color:white;
      opacity:0.8;
      width:100%;
      height:10vh;
      position:absolute;
      top:23vh;
      right:-55%;
      font-size:30px;
      color:#eb9f52;
      text-align:center;
      display:none;
      z-index:2;
    }
    .searchBlank input:first-child{
      width:94%;
      height:80%;
      position:absolute;
      left:3%;
      border:0px;
      top:10%;
      border:1px solid blue;
      border-width:1px 0px;
      opacity:1
    }
    .searchBlank input:nth-child(2){
      position:absolute;
      left:100%;
    }
    .searchRightside{
      width:60%;
      height:80vh;
      position:absolute;
      left:35%;
      top:10vh;
      border:2px solid white;
      overflow:scroll;
      overflow-x:hidden;
    }
    .searchRightside ul{
      list-style:square;
      color:white;
    }
    .searchRightside li{
      line-height:30px;
      font-size:15px;
      letter-spacing:2px;
      margin:10px;
      cursor:pointer;
      position:relative;
      left:0px;
    }
    .searchRightside a, .searchRightside a:hover, .searchRightside a:visited{
      color:white;
    }
    .newsPutTop{
      background-color:#eb9f52;
      color:white;
      font-size:15px;
      padding:2px;
      margin-right:5px;
    }
    .newsPutDate{
      font-size:10px;
      letter-spacing:3px;
      position:relative;
      top:2vh;
      left:3%;
    }
  </style>
</head>
<?php
  /*session_start();
  $_SESSION['bigDataBack']=1;*/
?>
<body>
  <div class="searchLeftside">
    <div class="searchTitle">搜尋公告</div>
    <div class="line"></div>
    <div class="keyword"><?php echo $_GET['s'];?></div>
    <span class="search glyphicon glyphicon-search"></span>
    <form class="searchBlank" method="get" role="search" action="<?php echo home_url('/')?>">
			<input type="text" value="| ENTER SEARCH KEYWORD." name="s"/>
		</form>
    <script>
      var searchClick=false;
      $(".search").click(function(){
        if(searchClick==false){
          $(".searchBlank").css({"display":"block"});
          $(".searchBlank input:eq(0)").val("| ENTER SEARCH KEYWORD.");
          $(".searchBlank input:eq(0)").select();
          searchClick=true;
        }
        else{
          $(".searchBlank").css({"display":"none"});
          searchClick=false;
        }
       });
    </script>
  </div>
  <div class="searchRightside">
    <ul>
			<?php query_posts('showposts=999 & s='.$_GET['s']);?>
      <?php $index=0; while(have_posts()):the_post(); $index++;?>
      <span class="newsPutDate"><?php the_time("Y.n.j");?></span>
      <li><a href="<?php the_permalink()?>"><?php if($index==1)echo "<span class=newsPutTop>置頂</span>";the_title();?></a></li>
      <?php endwhile; ?>
    </ul>
  </div>
</body>
</html>