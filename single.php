<!DOCTYPE html>
<html>
<head>
  <style type="text/css">
    ::-webkit-scrollbar {
      width: 3px;
    }
    ::-webkit-scrollbar-track {
      background-color: black;
    }
    ::-webkit-scrollbar-thumb {
      background-color: white; 
    }
    @font-face{
      font-family:'Noto Sans CJK TC';
      src:url('http://127.0.0.1/wp-content/themes/bigdata/assets/NotoSansMonoCJKtc-Bold.otf')
    }
    html,body{
      overflow:hidden;
      font-family:'Noto Sans CJK TC';
    }
    .aNews{
      position:absolute;
      top:0px;
      left:0px;
      width:100%;
      height:100vh;
      background-color:#005CAF;
    }
    .aNewsLeftside{
      height:100vh;
      width:30%;
      background-color:#eb9f52;
    }
    .title{
      color:white;
      position:absolute;
      width:25%;
      top:20vh;
      left:2%;
      font-size:50px;
      border:2px solid white;
    }
    .aNewsLeftside .goBackArrow{
      display: inline-block;  
      margin: 72px;  
      border-top: 2px solid white;
      border-right: 5px solid white;  
      width: 360px;
      height: 80px;  
      transform: rotate(180deg) skewX(-45deg);
      position:absolute;
      left:5%;
      top:2vh;
    }
    .aNewsLeftside .goBackText{
      position:absolute;
      color:white;
      font-size:50px;
      left:15%;
      top:15vh;
    }
    .aNewsLeftside #goBack{
      cursor:pointer;
    }
    .aNewsRightside{
      width:60%;
      height:80vh;
      position:absolute;
      top:10vh; 
      left:35%;
      color:white;
      border:2px solid white;
    }
  </style>
  <script src="<?php bloginfo('template_directory')?>/js/jquery-3.3.1.min.js"></script>
</head>
<body>
  <div class="aNews">
    <div class="aNewsLeftside">
      <a href="https://bdrc.nctu.edu.tw/#news_"><img src="<? bloginfo('template_directory')?>/assets/bdrc_news_backtolastpage.png"/></a>
      <span class="title">
        <?php echo apply_filters('the_title', $post->post_title);?>
      </span> 
    </div>
    <div id="aNewsRightside" class="aNewsRightside">
      <?php echo apply_filters('the_content', $post->post_content); ?>
    </div>
  </div>
  <script>
    const beforePageLeft=25;
    function horiAnime(firstSelect, secondSelect, nowPageLeft){
      const firstLeft=parseInt(firstSelect.css("left"));
      const firstWidth=parseInt(firstSelect.css("width"));
      const secondLeft=parseInt(secondSelect.css("left"));
      const secondWidth=parseInt(secondSelect.css("width"));
      const nowPageLeftWidth=parseInt(nowPageLeft.css("width"));
      nowPageLeft.css({"width":beforePageLeft+"%"});
      firstSelect.css({"left":(firstLeft-firstWidth)+"px", "opacity":"0"});
      secondSelect.css({"left":(secondLeft-secondWidth)+"px", "opacity":"0"});
      nowPageLeft.animate({width:nowPageLeftWidth+"px"}, 1000, function(){
        firstSelect.animate({left:firstLeft+"px", "opacity":"1"}, 1000, function(){
          secondSelect.animate({left:secondLeft+"px", "opacity":"1"}, 1000);
        });
      })
    }
    horiAnime($(".title"), $("#aNewsRightside"), $(".aNewsLeftside"));
  </script>
</body>
</html>