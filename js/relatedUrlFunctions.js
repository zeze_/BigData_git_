function chooseWhat(){
  for(var i=0; i<3; i++){
    $("#url"+i).css({"display":"none"});
    $("#relatedUrlLeft li:eq("+i+")").click({urlIndex:i}, function(e){
      for(var j=0; j<3; j++){
        $("#url"+j).css({"display":"none"});
        $("#relatedUrlLeft li:eq("+j+")").css({"text-decoration":"none", "list-style":"circle"});
      }
      var relatedImg=document.getElementById("relatedImg");
      relatedImg.src=relatedImgSrc[e.data.urlIndex];
      relatedImg.width=relatedImgWidth[e.data.urlIndex]+"px";
      relatedImg.height=relatedImgHeight[e.data.urlIndex]+"px";
      $("#url"+e.data.urlIndex).css({"display":"block"});
      $("#relatedUrlLeft li:eq("+e.data.urlIndex+")").css({"text-decoration":"underline", "list-style":"square"});
    })
  }
  $("#relatedUrlLeft li:eq(0)").click();
}
function relatedUrlPrepareStart(){
  chooseWhat();
}