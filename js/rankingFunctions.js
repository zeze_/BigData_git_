//iframe設定
var rankingClickCheck=false;
var rankingUrl, rankingIframe;
function iframeSetting(){
  rankingUrl=["https://datastudio.google.com/embed/reporting/1i5fxaThFZOTtLeAwZ5H5ua7ewGJJDwS7/page/7EYU", 
              "https://rankings.betteredu.net/THE/Asia/2018.html",
              "https://www.usnews.com/best-colleges/rankings/national-universities",
              "https://www.qianmu.org/ranking/904.htm"];
  rankingIframe=document.getElementById("rankingIframe");
}
//點選世界大學排名中的哪一項
function chooseWhat(){
  var allRankingPage=4;
  for(var i=0; i<allRankingPage; i++){
    $("#rankingLeftside li:eq("+i+")").click({allRankingPageIndex:i}, function(e){
      rankingIframe.src=rankingUrl[e.data.allRankingPageIndex];
      for(var j=0; j<allRankingPage; j++){
        if(e.data.allRankingPageIndex==j){
          $("#rankingLeftside li:eq("+j+")").css({"list-style":"square"});
        }
        else{
          $("#rankingLeftside li:eq("+j+")").css({"list-style":"circle"});
        }
      }
    });
  }
}
function rankingPrepareStart(){
  iframeSetting();
  chooseWhat();
}