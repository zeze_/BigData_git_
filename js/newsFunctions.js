//預設照片
function getDefaultPhotos(){
  newsPhoto=document.getElementById("newsPhoto");
  if(newsPhotoArr[0]==undefined)newsPhotoArr.push(default1, default2, default3);
  else if(newsPhotoArr[1]==undefined)newsPhotoArr.push(default1, default2);
  else if(newsPhotoArr[2]==undefined)newsPhotoArr.push(default1);
}
//輪播照片和切換照片
var clickPhotoButton=0;
var newsPhotoTotal=3;
var newsPhotoLeft=new Array(45, 52, 59);
var newsPhotoArr=[];
var newsPhoto;
var loadPhoto=false;
function getNewsPhoto(){  
  for(var i=0; i<newsPhotoTotal; i++){
    document.write("<div class=newsPhotoChange style=left:"+newsPhotoLeft[i]+"%><div></div></div>");
  }
}
function changePhoto(){
  loadPhoto=true;
  for(var i=0; i<newsPhotoTotal; i++){
    $(".newsPhotoChange:eq("+i+")").click({newsPhotoIndex:i}, function(e){
      $(".newsPhotoChange div").css({"height":"1px"});
      $(".newsPhotoChange div:eq("+e.data.newsPhotoIndex+")").css({"height":"3px"});
      clickPhotoButton=e.data.newsPhotoIndex;
      newsPhoto.src=newsPhotoArr[e.data.newsPhotoIndex];
    });
  }
  $(".newsPhotoChange:eq(0)").click();
  setInterval(function(){
    $(".newsPhotoChange div").css({"height":"1px"});
    $(".newsPhotoChange div:eq("+clickPhotoButton+")").css({"height":"3px"});
    newsPhoto.src=newsPhotoArr[clickPhotoButton];
    clickPhotoButton++;
    if(clickPhotoButton>2)clickPhotoButton=0;
  },2000);

  //下面的小裝飾半圓形
  var decoration=document.getElementById("decoration");
  decoration.src=decorationUrl;
}
//最新公告_搜尋
var searchClick=false, allNewsSearchClick=false;
function newsSearch(){
  $("#searchIcon, #searchText").click(function(){
    if(searchClick==false){
      $("#searchBlank").slideDown();
      $("#searchBlank input:eq(0)").select();
      searchClick=true;
    }
    else{
      $("#searchBlank").slideUp();
      searchClick=false;
    }
  });
  $("#allNewsSearch").click(function(){
    if(allNewsSearchClick==false){
      $("#searchAllNewsBlank").slideDown();
      $("#searchAllNewsBlank input:eq(0)").select();
      allNewsSearchClick=true;
    }
    else{
      $("#searchAllNewsBlank").slideUp();
      allNewsSearchClick=false;
    }
  })
}
//所有公告-點選某分類
function allNewsChooseCat(){
  var allNewsIndexNum=4;
  var pageId=[7, 3, 4, 6];
  var allNewsBlockRightside=document.getElementById("allNewsBlockRightside");
  for(var i=0; i<allNewsIndexNum; i++){
    $("#menu-allnewsblock li:eq("+i+")").click({chosenIndex:i}, function(e){
      switch(e.data.chosenIndex){
        case 0:{
          allNewsBlockRightside.innerHTML=cat0;
          break;
        }
        case 1:{
          allNewsBlockRightside.innerHTML=cat1;
          break;
        }
        case 2:{
          allNewsBlockRightside.innerHTML=cat2;
          break;
        }
        case 3:{
          allNewsBlockRightside.innerHTML=cat3;
          break;
        }
      }
      $("#menu-allnewsblock li").css({"list-style":"circle"});
      $("#menu-allnewsblock li:eq("+e.data.chosenIndex+")").css({"list-style":"square"});
    })
  }
  if(pageIdFromPhp){
    allNewsBlockRightside.innerHTML=pageNavi;
    $("#menu-allnewsblock li:eq("+pageIndex+")").css({"list-style":"square"});
  }
  else $("#menu-allnewsblock li:eq(0)").click();
  $("#allNewsBlockRightside").mouseover(function(){
    onOtherScroll=true;
  })
  $("#allNewsBlockRightside").mouseout(function(){
    onOtherScroll=false;
  })
}
function newsPrepareStart(){
  getDefaultPhotos();
}
function newsReadyStart(){
  newsSearch();
  $("#changeAllNewsIcon").click(function(){
    $("#bigDataFullPage").fullpage.moveSlideRight();
  })
  $("#changeAllNewsText2").click(function(){
    $("#bigDataFullPage").fullpage.moveSlideRight();
  })
}