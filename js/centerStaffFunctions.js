//點選人員簡介中的哪一項
function chooseWhat(){
  var allStaffPage=3;
  var centerId=[];
  for(var i=0; i<allStaffPage; i++){
    centerId.push(document.getElementById("center"+i))
    $("#staffLeftside li:eq("+i+")").click({allStaffPageIndex:i}, function(e){
      for(var j=0; j<allStaffPage; j++){
        centerId[j].style.display="none";
        if(e.data.allStaffPageIndex==j){
          $("#staffLeftside li:eq("+j+")").css({"list-style":"square"});
          centerId[j].style.display="block";
        }
        else{
          $("#staffLeftside li:eq("+j+")").css({"list-style":"circle"});
        }
      }
    });
  }
}
//切換中心簡介與人員簡介
function changeTo(){
  $("#changeToStaff").click(function(){
    $("#bigDataFullPage").fullpage.moveSlideRight();
  })
  $("#changeToCenterInform").click(function(){
    $("#bigDataFullPage").fullpage.moveSlideLeft();
  })
}
function staffPrepareStart(){
  chooseWhat();
}
function staffReadyStart(){
  changeTo();
}
//load picture
var staffPicture=false;
function loadStaffPicture(){
  var center0pictureId=document.getElementById("center0picture");
  var center1pictureId=document.getElementById("center1picture");
  center0pictureId.src=center0picture;
  center1pictureId.src=center1picture;
  staffPicture=true;
}