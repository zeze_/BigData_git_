//首頁title從大變小
var sizeStart=new Array(75, 150, 45);
var sizeLast=new Array(59, 75, 38);
function homeTitleInit(){
  for(var i=0; i<3; i++){
    $("#homeTitle span:eq("+i+")").css({"font-size":sizeStart[i]+"px", "opacity":0});
  }
}
//首頁底下虛線特效
var right=2;
var bottom=new Array(41, 38, 34, 29, 24, 19, 14, 8);
var opacity=new Array(0.5, 0.5, 0.4, 0.4, 0.3, 0.3, 0.2, 0.1);
var lineHeight_=new Array(12, 12, 20, 20, 28, 28, 28, 28);
var m=0;
var straightQueue=[];
for(var j=0; j<40; j++){
  var fun=function(n){
    for(var i=0; i<8; i++){
      $("#dash"+i+n).css({"display":"block"}, 0);
      $("#dash"+i+n).animate({width:1.25+"%"}, 100);
      
    }
  }
  straightQueue.push(fun);
}
function straight(){
  var straightInterval=setInterval(function(){
    if(m>=39)clearInterval(straightInterval);
    straightQueue[m](m);
    m++;
    if(m==6){
      for(var i=0; i<20; i++){
        $("#barChart"+i).animate({height:barHeight[i]+"vh"}, 500);
      }
    }
  }, 50);
}
//虛線特效的背景
function dashLineBackgroundInit(){
  $("header #dashLineBackground").slideUp(0);
}
function dashLineBackgroundDown(){
  $("header #dashLineBackground").slideDown(1000, straight());
}
//柱狀圖
var barRight=new Array(20);
var barBottomTop=new Array(20);
var barHeight=new Array(16, 27, 27, 29, 19, 21, 22, 22, 26, 25, 23, 12, 28, 25, 26, 32, 20, 25, 16, 20);
barRight[0]=13;
barBottomTop[0]=60;
for(var i=1; i<20; i++){
  if(i%2==0){
    barRight[i]=barRight[i-1]+0.5+0.8;
    barBottomTop[i]=60;
  }
  else{
    barRight[i]=barRight[i-1]+0.6;
    barBottomTop[i]=33;
  }
}
//折線圖
var lineWidth=new Array(15, 23, 30, 36, 43, 50, 58, 66, 73, 80, 87, 95);
var lineHeight=new Array(96, 88, 70, 67, 52, 59, 54, 56, 36, 44, 42, 20);
var lineWidth2=new Array(15, 23, 30, 37, 42, 50, 58, 66, 73, 80, 87, 95);
var lineHeight2=new Array(96, 92, 80, 86, 65, 72, 64, 72, 65, 62, 76, 40);
function lineChart(lineWidth, lineHeight, n){
  var homeCanvas=document.getElementById('homeCanvas'+n);
  var homeCanvasContext=homeCanvas.getContext('2d');
  homeCanvasContext.beginPath();
  homeCanvasContext.imageSmoothingEnabled = true;
  homeCanvas.width=windowWidth;
  homeCanvas.height=windowHeight;
  var homeCanvasWidth =screen.width-20;
  var homeCanvasHeight=homeCanvas.height-20;
  if (window.devicePixelRatio) {
    homeCanvas.style.width = homeCanvasWidth + "px";
    homeCanvas.style.height = homeCanvasHeight + "px";
    homeCanvas.height = homeCanvasHeight * window.devicePixelRatio;
    homeCanvas.width = homeCanvasWidth * window.devicePixelRatio;
    homeCanvasContext.scale(window.devicePixelRatio, window.devicePixelRatio);
  }
  homeCanvasContext.strokeStyle='white';
  homeCanvasContext.lineWidth=4;
  homeCanvasContext.moveTo(homeCanvasWidth*15/100, homeCanvasHeight*96/100);
  var nowLineWidth=15, nowLineHeight=96;
  var lineCount=0;
  var intervalWidth=(lineWidth[1]-lineWidth[0])/20;
  var intervalHeight=(lineHeight[1]-lineHeight[0])/20;
  var lineStop=0;
  var lineInterval=setInterval(function(){
    if(lineCount>10)clearInterval(lineInterval);
    if(lineStop>=20){
      lineCount++;
      lineStop=0;
      intervalWidth=(lineWidth[lineCount+1]-lineWidth[lineCount])/20;
      intervalHeight=(lineHeight[lineCount+1]-lineHeight[lineCount])/20;
    }
    nowLineWidth+=intervalWidth;
    nowLineHeight+=intervalHeight;
    homeCanvasContext.lineTo(homeCanvasWidth*nowLineWidth/100, homeCanvasHeight*nowLineHeight/100);
    homeCanvasContext.stroke();
    lineStop++;
  }, 10);
}
//選單三角形
var triangleLeft=new Array();
function headerMenuTriangle(){
  subMenuLength=$("header .sub-menu").length;
  for(var i=0; i<subMenuLength; i++){
    var div=document.createElement("div");
    $("header .menu-item-has-children:eq("+i+")").append(div);
    $("header .menu-item-has-children:eq("+i+") div").addClass("arrow-up");
  }
  //調整裡面li位置
  for(var i=0; i<$("header .sub-menu").length; i++){
    $("header .sub-menu:eq("+i+") li:eq(0)").css({"margin-left":10.5*(i+1)+"%"});
    $("nav .sub-menu:eq("+i+") li:eq(0)").css({"margin-left":10.5*(i+1)+"%"});
  }
}
//預備特效
function homePrepareStart(){
  //menu上三角形
  headerMenuTriangle();
  //虛線
  for(var j=0; j<8; j++){
  for(var i=0; i<40; i++){
      document.write("<div id='dash"+j+i+"' class='dashLine' style='right:"+i*2.5+"%'></div>");
      document.getElementById("dash"+j+i).style.bottom=bottom[j]+"vh";
      document.getElementById("dash"+j+i).style.lineHeight=lineHeight_[j]+"px";
      document.getElementById("dash"+j+i).style.opacity=opacity[j];
      right+=2.5;
    }
  }
  //柱狀圖
  for(var i=0; i<20; i++){
    document.write("<div id=barChart"+i+" class=barChart></div>");
    document.getElementById("barChart"+i).style.right=barRight[i]+"%";
    if(i%2==0){
      document.getElementById("barChart"+i).style.top=barBottomTop[i]-5+"vh";
    }
    else{
      document.getElementById("barChart"+i).style.bottom=barBottomTop[i]+5+"vh";
    }
  }
}
//執行特效
function homeReadyStart(){
  homeTitleInit();
  dashLineBackgroundInit();
  for(var i=0; i<3; i++){
    if(i==0){
      $("#homeTitle span:eq("+i+")").animate({fontSize:sizeLast[i]+"px", opacity:1}, 1000, function(){dashLineBackgroundDown();});
    }
    else{
      $("#homeTitle span:eq("+i+")").animate({fontSize:sizeLast[i]+"px", opacity:1}, 1000);
    }
  }
  lineChart(lineWidth, lineHeight, 1);
  lineChart(lineWidth2, lineHeight2, 2);
}
