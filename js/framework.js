//螢幕長寬
var windowWidth=window.innerWidth;
var windowHeight=window.innerHeight;
var orangeBack=0;

p2_2=true;
p3_2=true;
switch_animationing=false;
//換頁動畫
var lastPageBack=0;
function switchAnimate(background, leftside, rightside){
  switch_animationing=true;
  const backgroundWidth=parseInt(background.css("width"));
  const leftsideLeft=parseInt(leftside.css("left"));
  const leftsideWidth=parseInt(leftside.css("width"));
  const rightsideLeft=parseInt(rightside.css("left"));
  const rightsideWidth=parseInt(rightside.css("width"));
  background.css({"width":lastPageBack+"px"});
  leftside.css({"left":(leftsideLeft-leftsideWidth)+"px", "opacity":"0"});
  rightside.css({"left":(rightsideLeft-rightsideWidth)+"px", "opacity":"0"});
  $("#bigDataFullPage").fullpage.setAllowScrolling(false, 'up ,down, left, right');
  background.animate({width:backgroundWidth+"px"}, 1000, function(){
    leftside.animate({left:leftsideLeft+"px", "opacity":"1"}, 1000, function(){
      rightside.animate({left:rightsideLeft+"px", "opacity":"1"}, 1000, function(){
        $("#bigDataFullPage").fullpage.setAllowScrolling(true, 'up, down, left, right');
      });
      lastPageBack=backgroundWidth;
      switch_animationing=false;
    })
  })
}

$(document).ready(function(){
  $('.section, nav').css({"display":"block"});
  homeReadyStart();
  newsReadyStart();
  staffReadyStart();
  $( "#bigDataFullPage" ).fullpage({
    autoScrolling: true,
    anchors: ["header_", "news_", "center-inform", "public_", "ranking_", "newsletter_", "contact_", "related-url", "sitemap_"],
    navigation: false, // 顯示導行列
    // scrollHorizontally: true, // 導行列位置
    sectionsColor: ["#0070af", "#0070af", "#0070af", "#0070af", "white", "#0070af", "#0070af", "#0070af", "#0070af"], //各section顏色
    scrollingSpeed:0, //滑行速度
    lockAnchors: false,
    // animateAnchor: true,
    // recordHistory: false,
    // lazyLoading: true,
    // css3: true,
    lockAnchors: true,
    afterLoad: function(origin, destination, direction){
      if(destination==1){
        $("#homeTriangle").css({"display":"none"});
        $("nav").css({"display":"none"});
        lastPageBack=0;
      }
      else{
        $("#homeTriangle").css({"display":"block"});
        $("nav").css({"display":"block"});
      }
      
      if(destination==2){
        if(!loadPhoto)changePhoto();
        if(!switch_animationing){
          if(p2_2)switchAnimate($("#recentNews .leftsideBackground"), $("#newsLeftside"), $("#newsRightside"));
          else switchAnimate($("#allNewsBlock .leftsideBackground"), $("#allNewsBlockLeftside"), $("#allNewsBlockRightside"));
        }
      }
      else if(destination==3){
        var relationPictureId=document.getElementById("relationPicture");
        relationPictureId.src=relationPicture;
        if(!staffPicture)loadStaffPicture();
        $("#staffLeftside li:eq(0)").click();
        if(!switch_animationing){
          if(p3_2)switchAnimate($("#centerInform .leftsideBackground"), $("#centerInformLeftside"), $("#centerInformRightside"));
          else switchAnimate($("#staff .leftsideBackground"), $("#staffLeftside"), $("#staffRightside"));
        }
      }
      else if(destination==4){
        switchAnimate($("#public .leftsideBackground"), $("#publicLeftside"), $("#staffRightside"));
      }
      else if(destination==5){
        if(!rankingClickCheck)$("#rankingLeftside li:eq(0)").click();
        rankingClickCheck=true;
        switchAnimate($("#ranking .leftsideBackground"), $("#rankingLeftside"), $("#rankingIframeBlock"));
      }
      else if((destination==6)){
        switchAnimate($("#newsletter .leftsideBackground"), $("#newsletterLeft"), $("#newsletterRight"));
      }
      else if(destination==7){
        $("#contactPictureChange1").click();
        switchAnimate($("#contact .leftsideBackground"), $("#contactLeftside"), $("#contactRightside"));
      }
      else if(destination==8){
        switchAnimate($("#relatedUrl .leftsideBackground"), $("#relatedUrlLeft"), $("#relatedUrlRight"));
      }
      
    },
    afterResize: function(width, height){},
    afterSlideLoad: function(section, origin, destination, direction){
      
    },
    onSlideLeave: function(section, origin, destination, direction){
      
    },
    onLeave: function(index, nextIndex, direction){

    }
  });
  footerReadyStart();
  

  //nav & header menu & 一些按鈕
  $("#menu-navmenu .sub-menu:eq(0) li:eq(0), #menu-headermenu .sub-menu:eq(0) li:eq(0), #menu-headermenu > li:eq(0), #menu-navmenu > li:eq(0), #changeNewsText2").click(function(){
    if(!switch_animationing){
      p2_2=true;
      switch_animationing=true;
      $("#bigDataFullPage").fullpage.moveTo("news_", 1);
      switchAnimate($("#recentNews .leftsideBackground"), $("#newsLeftside"), $("#newsRightside"));
    }
  })
  $("#menu-navmenu .sub-menu:eq(0) li:eq(1), #menu-headermenu .sub-menu:eq(0) li:eq(1), #changeAllNewsText2").click(function(){
    if(!switch_animationing){
      p2_2=false;
      switch_animationing=true;
      $("#bigDataFullPage").fullpage.moveTo("news_", 2);
      switchAnimate($("#allNewsBlock .leftsideBackground"), $("#allNewsBlockLeftside"), $("#allNewsBlockRightside"));
    }
  })
  $("#menu-navmenu .sub-menu:eq(1) li:eq(0), #menu-headermenu .sub-menu:eq(1) li:eq(0), #changeToCenterInform, #menu-headermenu > li:eq(1), #menu-navmenu > li:eq(1)").click(function(){
    if(!switch_animationing){
      p3_2=true;
      switch_animationing=true;
      $("#bigDataFullPage").fullpage.moveTo("center-inform", 1);
      switchAnimate($("#centerInform .leftsideBackground"), $("#centerInformLeftside"), $("#centerInformRightside"));
    }
  })
  $("#menu-navmenu .sub-menu:eq(1) li:eq(1), #menu-headermenu .sub-menu:eq(1) li:eq(1), #changeToStaff").click(function(){
    if(!switch_animationing){
      p3_2=false;
      switch_animationing=true;
      $("#bigDataFullPage").fullpage.moveTo("center-inform", 2);
      switchAnimate($("#staff .leftsideBackground"), $("#staffLeftside"), $("#staffRightside"));
    }
  })
  $("#menu-navmenu .sub-menu:eq(2) li:eq(0), #menu-headermenu .sub-menu:eq(2) li:eq(0), #menu-headermenu > li:eq(2), #menu-navmenu > li:eq(2)").click(function(){
    if(!switch_animationing)$("#bigDataFullPage").fullpage.moveTo("public_", 1);
  })
  $("#menu-navmenu .sub-menu:eq(2) li:eq(1), #menu-headermenu .sub-menu:eq(2) li:eq(1)").click(function(){
    if(!switch_animationing)$("#bigDataFullPage").fullpage.moveTo("public_", 1);
  })
  $("#menu-navmenu .sub-menu:eq(3) li:eq(0), #menu-headermenu .sub-menu:eq(3) li:eq(0), #menu-headermenu > li:eq(3), #menu-navmenu > li:eq(3)").click(function(){
    if(!switch_animationing)$("#bigDataFullPage").fullpage.moveTo("ranking_", 1);
    $("#rankingLeftside li:eq(0)").click();
  })
  $("#menu-navmenu .sub-menu:eq(3) li:eq(1), #menu-headermenu .sub-menu:eq(3) li:eq(1)").click(function(){
    if(!switch_animationing)$("#bigDataFullPage").fullpage.moveTo("ranking_", 1);
    $("#rankingLeftside li:eq(1)").click();
  })
  $("#menu-navmenu .sub-menu:eq(3) li:eq(2), #menu-headermenu .sub-menu:eq(3) li:eq(2)").click(function(){
    if(!switch_animationing)$("#bigDataFullPage").fullpage.moveTo("ranking_", 1);
    $("#rankingLeftside li:eq(2)").click();
  })
  $("#menu-navmenu .sub-menu:eq(3) li:eq(3), #menu-headermenu .sub-menu:eq(3) li:eq(3)").click(function(){
    if(!switch_animationing)$("#bigDataFullPage").fullpage.moveTo("ranking_", 1);
    $("#rankingLeftside li:eq(3)").click();
  })
  $("#menu-navmenu > li:eq(4), #menu-headermenu > li:eq(4)").click(function(){
    if(!switch_animationing) $("#bigDataFullPage").fullpage.moveTo("newsletter_", 1);
  })
  $("#menu-navmenu > li:eq(5), #menu-headermenu > li:eq(5)").click(function(){
    if(!switch_animationing)$("#bigDataFullPage").fullpage.moveTo("contact_", 1)
  })
  $("#menu-navmenu > li:eq(6), #menu-headermenu > li:eq(6)").click(function(){
    if(!switch_animationing)$("#bigDataFullPage").fullpage.moveTo("related-url", 1)
  })
  $("#menu-navmenu > li:eq(7), #menu-headermenu > li:eq(7)").click(function(){
    if(!switch_animationing)$("#bigDataFullPage").fullpage.moveTo("sitemap_", 1);
  })
})
