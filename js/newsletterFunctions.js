var newsletterList=[], newsletterContent=[];
function initNewsletter(){
  $(".nowLookingNewsletterTitle, .nowLookingNewsletterContent, #pastNewsletterTitle").css({"display":"none"});
  $(".nowLookingNewsletterTitle:eq(0), .nowLookingNewsletterContent:eq(0)").css({"display":"block"});
}

function newsletterPrepareStart(){
	initNewsletter();
	$("#newsletterList li:eq(2)").click(function(){
		$("#newsletterList li:eq(0)").html("當期電子報:"+newsletterList[5]);
	});
	$("#clickPastNewsletter").click(function(){
		$("#pastNewsletterTitle").css({"display":"block"});
		$(".nowLookingNewsletterContent").css({"display":"none"});
	});
	for(var i=0; i<$(".pastNewsletterTitle").length; i++){
		$("#pastNewsletterTitle"+i).click({id:i}, function(e){
			$("#newsletterContent"+e.data.id).css({"display":"block"});
			$("#pastNewsletterTitle").css({"display":"none"});
		});
	}
}

/*
<?php 
						wp_reset_query();
						query_posts('paged=1 & cat='.$allId[4].' & showposts=1000'); 
						while(have_posts()):the_post();
					?>
					<script>
						newsletterList.push(`<?php the_title()?>`);
						newsletterContent.push("<?php echo apply_filters('the_content', $post->post_content);?>");
					</script>
					<?php endwhile;?>
*/