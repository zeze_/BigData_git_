//變換圖片
function switchPicture(){
  var contactPictureChangeLeft=[35, 45, 55];
  var contactPictureChangeTitle=["交通指南", "交大地圖", "工五地圖"];
  var contactPicture=document.getElementById("contactPicture");
  var contactPictureArr=[];
  contactPictureArr.push(contactPicture1);
  contactPictureArr.push(contactPicture2);
  for(var i=0; i<3; i++){
    document.write("<div id=contactPictureChange"+i+" class=contactPictureChange style=left:"+contactPictureChangeLeft[i]+"%>"+contactPictureChangeTitle[i]+"</div>");
    if(i==0){
      //點選交通指南
      $(".trafic").slideUp(0);
      $("#contactPictureChange0").click(function(){
        $(".trafic").stop(true, true).delay(50).slideDown(400);
        $("#trafic").children().mouseleave(function(){
          $(".trafic").stop(true, true).delay(50).slideUp(400);
        })
      })
    }
    else{
      //點選某個地圖
      $("#contactPictureChange"+i).click({contactPictureIndex:(i)}, function(e){
        $(".contactPictureChange").css({"text-decoration":"none"});
        $(".contactPictureChange:eq("+e.data.contactPictureIndex+")").css({"text-decoration":"underline"});
        contactPicture.src=contactPictureArr[e.data.contactPictureIndex-1];
      })
    }
  }
}
function contactPrepareStart(){
  switchPicture();
}