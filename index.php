<?php get_header();?>
<!---+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ home-->
	<div id="bigDataFullPage">
		<script src="<?php echo $rootDir?>/js/homeFunctions.js"></script>
		<div class="section" id="home">
			<canvas id="homeCanvas1"></canvas>
			<canvas id="homeCanvas2"></canvas>
			<header class="newPage">
				<div style="position:absolute; width:100%;">
					<img id="headerLogo" class="logo" src="<?php bloginfo('template_directory')?>/assets/NCTU_LOGO.png">
					<?php wp_nav_menu(array('theme_location'=>'header-menu')); ?>
				</div>
				<div id="taiwanEnglish">TAIWAN &nbsp;&nbsp;&nbsp;&nbsp;ENGLISH</div>
				<div id="dashLineBackground"></div>
				<script>homePrepareStart()</script>
				<div id="homeTitle">
					<a href="<?php bloginfo('url'); ?>">
						<div>
							<span>國立交通大學</span>
							<br>
							<span>大數據研究中心</span>
							<br>
							<span>Big Data Research Center</span>
						</div>
					</a>
				</div>
			</header>
		</div>
<!---+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ news-->
	<div class="section">
		<script src="<?php echo $rootDir?>/js/footerFunctions.js"></script>
		<script src="<?php echo $rootDir?>/js/newsFunctions.js"></script>
		<div class="slide newPage" data-anchor="1" id="recentNews">
		<div class="page1">
				<div class="leftsideBackground"></div>
				<div id="newsLeftside">
					<?php $post=get_post($newsLeftside)?>
					<div class="tagInit leftTop">
						<span id="newsTitle" class="title"><?php echo apply_filters('the_title', $post->post_title);?></span>
						<span id="changeAllNewsText2">所有公告</span>
						<span class="bottomLine"></span>
					</div>
					<img id="newsPhoto"/>
					<img id="decoration"/>
					<script>getNewsPhoto()</script>
				</div>
				<div id="newsRightside" class="rightsideBackground">
					<div id="searchText" class="tagInit">搜尋</div>
					<span class="glyphicon glyphicon-search tagInit" id="searchIcon"></span>
					<form id="searchBlank" class="blank" method="get" role="search" action="<?php echo home_url('/')?>">
						<input type="text" placeholder="ENTER SEARCH KEYWORD." name="s"/>
					</form>
					<div id="newsPut">
						<ul>
							<?php wp_reset_query()?>
							<?php query_posts('showposts=6&cat='.$allId[0]);?>
							<?php $index=0;while(have_posts()):the_post();$index++?>
							<script>if('<?php get_feature_image()?>'!='')newsPhotoArr.push('<?php get_feature_image();?>')</script>
							<span class="newsPutDate"><?php the_time("Y.n.j");?></span>
							<a href="<?php echo esc_url( apply_filters( 'the_permalink', get_permalink( $post ), $post ) )?>"><li><?php if($index==1)echo "<span class=newsPutTop>置頂</span>";the_title();?></li></a>
							<?php endwhile; ?>
							<script>
								var default1="<?php echo $rootDir?>/assets/default1.jpg";
								var default2="<?php echo $rootDir?>/assets/default2.jpg";
								var default3="<?php echo $rootDir?>/assets/default3.jpg";
								var decorationUrl="<?php echo $rootDir?>/assets/Oval7.png";
							</script>
						</ul>
					</div>
				</div>
				<div class="homeTriangle"></div>
			</div>
			<script>newsPrepareStart()</script>
		</div>
		<div class="slide newPage" data-anchor="2" id="allNews">
			<div id="allNewsBlock" class="page2">
				<div class="leftsideBackground"></div>
				<div id="allNewsBlockLeftside" class="block">
					<?php $post=get_post($allNewsBlockLeftside)?>
					<div class="tagInit leftTop">
						<span id="allNewsBlockTitle" class="title"><?php echo apply_filters('the_title', $post->post_title);?></span>
						<span id="changeNewsText2">最新公告</span>
						<span class="bottomLine"></span>
					</div>
					<span id="allNewsSearch" class="glyphicon glyphicon-search tagInit"></span>
					<form id="searchAllNewsBlank" class="blank" method="get" role="search" action="<?php echo home_url('/')?>">
						<input type="text" placeholder="ENTER SEARCH KEYWORD." name="s"/>
					</form>
					<?php wp_nav_menu(array('theme_location'=>'allNewsBlock')); ?>
				</div>
				<div id="allNewsBlockRightside">
					<?php wp_reset_query()?>
					<script>
						var pageIdFromPhp=<?php echo $pageId?>;
						var pageNavi="<ul>"+"<?php whichCat($allId[$pageIndex-1] ,$pageId);?>"+"<?php wp_pagenavi($pageIndex);?>"+"</ul>";
						var pageIndex=<?php echo $pageIndex-1?>;
						var cat0="<ul>"+"<?php whichCat($allId[0], 1, 10);?>"+"<?php wp_pagenavi(1);?>"+"</ul>";
						var cat1="<ul>"+"<?php whichCat($allId[1], 1, 10);?>"+"<?php wp_pagenavi(2);?>"+"</ul>";
						var cat2="<ul>"+"<?php whichCat($allId[2], 1, 10);?>"+"<?php wp_pagenavi(3);?>"+"</ul>";
						var cat3="<ul>"+"<?php whichCat($allId[3], 1, 10);?>"+"<?php wp_pagenavi(4);?>"+"</ul>";
						allNewsChooseCat()
					</script>
				</div>
				<div class="homeTriangle"></div>
			</div>
		</div>
	</div>
<!---+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ center inform-->
	<div class="section">
		<script src="<?php echo $rootDir?>/js/centerStaffFunctions.js"></script>
		<div class="slide newPage" data-anchor="1">
			<div id="centerInform" class="page1">
				<div class="leftsideBackground"></div>
				<div id="centerInformLeftside" class="block">
					<div class="tagInit leftTop">
						<?php $post=get_post($centerInformLeftside)?>
						<span id="centerInformTitle" class="title"><?php echo apply_filters('the_title', $post->post_title);?></span>
						<?php $post=get_post($staffLeftside);?>
						<span id="changeToStaff"><?php echo apply_filters('the_title', $post->post_title);?></span>
						<?php $post=get_post($centerInformLeftside)?>
						<div class="bottomLine"></div>
					</div>
					<script>var relationPicture="<?php echo wp_get_attachment_url( get_post_thumbnail_id() )?>";</script>
					<img id="relationPicture">
					<div id="centerInformFooter"><?php echo the_post_thumbnail_caption()?></div>
				</div>	
				<div id="centerInformRightside">
					<div id="centerInformRightsideContent"><?php echo apply_filters('the_content', $post->post_content);?></div>
				</div>
				<div class="homeTriangle"></div>
			</div>
		</div>
		<div class="slide newPage" data-anchor="2">
			<div id="staff" class="page2">
				<div class="leftsideBackground"></div>
				<div id="staffLeftside" class="block">
					<div class="leftTop">
						<?php $post=get_post($staffLeftside);?>
						<span id="staffTitle" class="title"><?php echo apply_filters('the_title', $post->post_title);?></span>
						<?php $post=get_post($centerInformLeftside)?>
						<span id="changeToCenterInform" class=""><?php echo apply_filters('the_title', $post->post_title);?></span>
						<?php $post=get_post($staffLeftside);?>
						<span class="bottomLine"></span>
					</div>
					<?php wp_nav_menu(array('theme_location'=>'staffInform')); ?>
				</div>
				<div id="staffRightside">
					<div id="center0">
						<?php $post=get_post($staffRightside_center0);?>
						<script>var center0picture="<?php get_feature_image();?>";</script>
						<img id="center0picture"/>
						<?php echo apply_filters('the_content', $post->post_content);?>
					</div>
					<div id="center1">
						<?php $post=get_post($staffRightside_center1);?>
						<script>var center1picture="<?php get_feature_image();?>";</script>
						<img id="center1picture"/>
						<?php echo apply_filters('the_content', $post->post_content);?>
					</div>
					<div id="center2"><?php $post=get_post($staffRightside_center2); echo apply_filters('the_content', $post->post_content);?></div>
				</div>
				<div class="homeTriangle"></div>
			</div>
		</div>
		<script>staffPrepareStart()</script>
	</div>
<!---+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ public-->
	<div class="section">
		<script src="<?php echo $rootDir?>/js/publicFunctions.js"></script>
		<div id="public" class="newPage">
			<div class="leftsideBackground"></div>
			<div id="publicLeftside" class="block">
				<div class="leftTop tagInit">
					<?php $post=get_post($publicLeftside)?>
					<div id="publicTitle" class="title"><?php echo apply_filters('the_title', $post->post_title);?><br>(建置中)</div>
					<div class="bottomLine"></div>
				</div>
				<?php wp_nav_menu(array('theme_location'=>'publicBlock')); ?>
			</div>
			<div id="publicRightside"></div>
			<script>publicPrepareStart();</script>
		</div>
		<div class="homeTriangle"></div>
	</div>
<!---+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ ranking-->
	<div class="section">
		<script src="<?php echo $rootDir?>/js/rankingFunctions.js"></script>
		<div id="ranking" class="newPage">
			<div class="leftsideBackground"></div>
			<div id="rankingLeftside" class="block">
				<div class="tagInit leftTop">
					<?php $post=get_post($rankingLeftside);?>
					<div id="rankingTitle" class="title"><?php echo apply_filters('the_title', $post->post_title);?></div>
					<div class="bottomLine"></div>
				</div>
				<?php wp_nav_menu(array('theme_location'=>'worldRank')); ?>
			</div>
			<div id="rankingIframeBlock">
				<iframe id="rankingIframe" src="" width="60%" style="height:100vh" scrolling="no"></iframe>
			</div>
			<script>rankingPrepareStart()</script>
		</div>
		<div class="homeTriangle" style="border-color:#005CAF transparent;"></div>
	</div>
<!---+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ newsletter-->
	<div class="section">
	<script src="<?php echo $rootDir?>/js/newsletterFunctions.js"></script>
		<div id="newsletter" class="newPage">
			<div class="leftsideBackground"></div>
			<div id="newsletterLeft" class="block">
				<div class="tagInit leftTop">
					<?php $post=get_post($newsletterLeft);?>
					<div id="newsletterTitle" class="title"><?php echo apply_filters('the_title', $post->post_title);?></div>
					<div class="bottomLine"></div>
				</div>
				<ul id="newsletterList" class="tagInit">
					<?php 
					 	wp_reset_query();
						query_posts('paged=1 & cat='.$allId[4].' & showposts=1000'); 
						$index=0;
					?>
					<li>當期電子報:<?php the_title();?></li>
					<?php while(have_posts()):the_post();?>
					<li id="newletterTitle<?php echo $index?>" class="nowLookingNewsletterTitle">現在觀看:<?php the_title();?></li>
					<?php
						$index++; 
						endwhile;
					?>
					<li id="clickPastNewsletter">往期電子報</li>
				</ul>
			</div>
			<div id="newsletterRight" class="tagInit">
				<?php 
					wp_reset_query();
					query_posts('paged=1 & cat='.$allId[4].' & showposts=1000'); 
					$index=0;
					while(have_posts()):the_post();
				?>
				<div id="newsletterContent<?php echo $index?>" class="nowLookingNewsletterContent"><?php the_content();?></div>
				<?php
					$index++;
					endwhile;
				?>
				<ul id="pastNewsletterTitle" class="tagInit">
					<?php 
					 	wp_reset_query();
						query_posts('paged=1 & cat='.$allId[4].' & showposts=1000'); 
						$index=0;
						while(have_posts()):the_post();
					?>
					<li id="pastNewsletterTitle<?php echo $index?>" class="pastNewsletterTitle"><?php the_title();?></li>
					<?php
						$index++; 
						endwhile;
					?>
				</ul>
			</div>
		</div>
		<div class="homeTriangle"></div>
		<script>newsletterPrepareStart();</script>
	</div>
<!---+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ contact-->
	<div class="section">
		<script src="<?php echo $rootDir?>/js/contactFunctions.js"></script>
		<div id="contact" class="newPage">
			<div class="leftsideBackground"></div>
			<div id="contactLeftside" class="block">
				<div class="leftTop">
					<?php $post = get_post($contactLeftside);?>
					<div id="contactTitle" class="title"><?php echo apply_filters('the_title', $post->post_title);?></div>
					<div class="bottomLine"></div>
				</div>
				<?php	echo apply_filters('the_content', $post->post_content);?>
			</div>
			<div id="contactRightside">
				<?php $post = get_post($contactRightside);?>
				<img id="contactPicture">
				<div id="trafic">
					<div class="trafic">
						<?php echo apply_filters('the_content', $post->post_content);?>
					</div>
				</div>
				<script>
					var contactPicture1="<?php bloginfo('template_directory')?>/assets/bdrc_contectus_map.png?>";
					var contactPicture2="<?php bloginfo('template_directory')?>/assets/EE5.png";
					contactPrepareStart();
				</script>
			</div>
		</div>
		<div class="homeTriangle"></div>
	</div>
<!---+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ related url-->
	<div class="section">
		<script src="<?php echo $rootDir?>/js/relatedUrlFunctions.js"></script>
		<script>
			var relatedImgSrc=["<?php bloginfo('template_directory')?>/assets/BDRC_related_connet_gov.png",
									 "<?php bloginfo('template_directory')?>/assets/BDRC_related_link_school.png",
									 "<?php bloginfo('template_directory')?>/assets/BDRC_related_link_org.png"
			];
			var relatedImgWidth=[175, 299, 175];
			var relatedImgHeight=[43, 104, 43];
		</script>
		<div id="relatedUrl" class="newPage">
			<div class="leftsideBackground"></div>
			<div id="relatedUrlLeft" class="block">
				<div class="leftTop">
					<?php $post=get_post($relatedUrlLeft)?>
					<div id="relatedUrlTitle" class="title"><?php echo apply_filters('the_title', $post->post_title);?></div>
					<div class="bottomLine"></div>
				</div>
				<?php echo apply_filters('the_content', $post->post_content);?>
			</div>
			<div id="relatedUrlRight">
				<div id="urls" class="tagInit">
					<img id="relatedImg" src="">
					<div id="url0"><?php wp_nav_menu(array('theme_location'=>'relatedUrl_government')); ?></div>
					<div id="url1"><?php wp_nav_menu(array('theme_location'=>'relatedUrl_campus')); ?></div>
					<div id="url2"><?php wp_nav_menu(array('theme_location'=>'relatedUrl_enterprise')); ?></div>
				</div>
			</div>
			<script>relatedUrlPrepareStart();</script>
		</div>
		<div class="homeTriangle"></div>
	</div>
<!---+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ sitemap-->
	<div class="section">
		<script src="<?php echo $rootDir?>/js/sitemapFunction.js"></script>
		<div id="sitemap" class="newPage">
		<div class="leftTop">
			<?php $post=get_post($sitemap)?>
			<div id="sitemapTitle" class="title"><?php echo apply_filters('the_title', $post->post_title);?></div>
		</div>
		<div class="homeTriangle"></div>
			
		</div>
	</div>
</body>
	
</div>


